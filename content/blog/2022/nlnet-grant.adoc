+++
title = "KiCad Project Awarded NLnet Grant"
date = "2022-09-06"
draft = false
"blog/categories" = [
    "News"
]
+++

The KiCad project is excited to announce that it has been awarded a 50K Euro
grant by the https://nlnet.nl/[NLnet Foundation].  This grant will fund the
development of the SPICE simulation improvements, design import/migration
wizard, CADSTAR library importer, database symbol libraries, footprint and
3D model wizard, flat schematics, and more.  Many of the features funded by
this grant will be available in the version 7.0 release of KiCad.  A detailed
list of the features and their development status can be found on the
https://gitlab.com/kicad/code/kicad/-/wikis/NLnet-funded-tasks[KiCad Developer's Wiki].
The KiCad project would like to thank the NLnet Foundation for their generosity.
