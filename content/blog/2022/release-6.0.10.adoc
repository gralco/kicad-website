+++
title = "KiCad 6.0.10 Release"
date = "2022-12-19"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.10 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 6.0.9 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/21[KiCad 6.0.10
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.10 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- https://gitlab.com/kicad/code/kicad/-/commit/d1bf889e712620b220b6d4fb68f6693c4a3903a4[Resolve embedded text variables].
- Keep keyboard placement on grid. https://gitlab.com/kicad/code/kicad/-/issues/13027[#13027]
- https://gitlab.com/kicad/code/kicad/-/commit/045633626c797a0bb3bba721ccdc054d8794bb1c[Clean up autosave file handling edge cases.]

=== Schematic Editor
- https://gitlab.com/kicad/code/kicad/-/commit/a22c4666394abf33546b40ce7ef1e8e55a22dd7b[Fix broken legacy schematic file save].
- https://gitlab.com/kicad/code/kicad/-/commit/d9a1703bcc6c9e041c49f4ab600a7150c2301601[Fix broken instance data loading legacy schematics].
- Fix unconnected bus from hierarchical sheet bug. https://gitlab.com/kicad/code/kicad/-/issues/12165[#12165]
- Prevent mirror of text elements. https://gitlab.com/kicad/code/kicad/-/issues/13007[#13007]
- Fix search for text containing slashes. https://gitlab.com/kicad/code/kicad/-/issues/12789[#12789]
- Add symbol library table mapping for rescue library on project save as. https://gitlab.com/kicad/code/kicad/-/issues/12503[#12503]
- Fix crash when "M" key pressed after clicking on end of unconnected wire. https://gitlab.com/kicad/code/kicad/-/issues/12936[#12936]
- https://gitlab.com/kicad/code/kicad/-/commit/9ac87132aff8699fa3e09f259b284c05117c9a78[Fix a issues with translated field names].
- https://gitlab.com/kicad/code/kicad/-/commit/5de05312d5181effd4016e2137b4b8efe445add4[Fix wxWidgets 3.2 assertion when opening symbol field table editor.]
- https://gitlab.com/kicad/code/kicad/-/commit/7f08356a48bfdaa5f06345b56cad4464247f707b[Fix NC and label logic.]
- https://gitlab.com/kicad/code/kicad/-/commit/948071183cf9b3d979a8bfbd407f4fb82b668b0e[Don't report a pin ERC if the pin is null].
- Prevent shared schematic UUIDs from changing when importing shared schematics. https://gitlab.com/kicad/code/kicad/-/issues/11076[#11076]

=== Symbol Editor
- Fix symbol SVG export. https://gitlab.com/kicad/code/kicad/-/issues/13093[#13093]

=== PCB Editor
- Return to original active layer after routing. https://gitlab.com/kicad/code/kicad/-/issues/12313[#12313]
- https://gitlab.com/kicad/code/kicad/-/commit/61d4a5bfbeb88fd9d6f7c3cbee465e77c034e8de[Resolve embedded text variables when project file is missing.]
- Fix an issue when importing SVG files using CR+LF as end of line. https://gitlab.com/kicad/code/kicad/-/issues/10096[#10096]
- https://gitlab.com/kicad/code/kicad/-/commit/2cf4cb937f2f9002a5ca1c42777959a33d19cf70[Fix graphic snapping].
- https://gitlab.com/kicad/code/kicad/-/commit/cca7f1dbda0c91e3b9372d874b8d96cc050f6e93[Use snap position for segment routability checks].
- Fix DRC crash when using negative clearance. https://gitlab.com/kicad/code/kicad/-/issues/12981[#12981]
- Fix DRC crash Pcbnew when moving ungroupable item to a group. https://gitlab.com/kicad/code/kicad/-/issues/13026[#13026]
- https://gitlab.com/kicad/code/kicad/-/commit/9d401b00d8061417bfb3a622bcb36842be668de4[Allow nested groups].
- https://gitlab.com/kicad/code/kicad/-/commit/3534cfbba8e74683a7461ad95ae47bcc15d4dc86[Allow a single net collision with a free pad].
- Honor zone fill minimum width setting. https://gitlab.com/kicad/code/kicad/-/issues/12797[#12797]
- Fix Z order of zone outline drawing. https://gitlab.com/kicad/code/kicad/-/issues/12438[#12438]
- Restore broken position relative to behavior. https://gitlab.com/kicad/code/kicad/-/issues/12912[#12912]
- Add support for unit-less values to custom rules evaluator. https://gitlab.com/kicad/code/kicad/-/issues/13016[#13016]
- Use correct pad to die lengths in differntial pair length tuner. https://gitlab.com/kicad/code/kicad/-/issues/12859[#12859]
- Fix an incorrect calculation in length tuning tools. https://gitlab.com/kicad/code/kicad/-/issues/12881[12881]
- https://gitlab.com/kicad/code/kicad/-/commit/c07043e762b06a04cdb779ee03837252435156dc[Respect the "report all errors" checkbox in DRC.]
- Fix crash when moving empty selection. https://gitlab.com/kicad/code/kicad/-/issues/13114[#13114]

=== Footprint Editor
- Fix crash when clicking on footprint properties button. https://gitlab.com/kicad/code/kicad/-/issues/12944[#12944]
- Fix crash when attempting to launch dimension tool. https://gitlab.com/kicad/code/kicad/-/issues/12841[#12841]

=== Gerber Viewer
- https://gitlab.com/kicad/code/kicad/-/commit/c3b9c65fd6fcad700a9c48737f6818d4e468a2b3[Don't generate error on paths when loading from zip archive].

=== Windows
- Fix icon scaling issue. https://gitlab.com/kicad/code/kicad/-/issues/11880[#11880]
- https://gitlab.com/kicad/code/kicad/-/commit/01cff24e4e53be1f7200d8caaeed28489ef6ad9a[Fix simulator crash due to old ngspice version].
