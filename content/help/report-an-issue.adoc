+++
title = "Report an Issue"
summary = "Guide on how to report issues to KiCad developers"
aliases = [ "/help/report-a-bug/", "/help/report-a-issue/" ]
[menu.main]
    parent = "Help"
    name   = "Report an Issue"
    weight = 35
+++
:linkattrs:


Select the appropriate location from the blue buttons below.  Once you are on the issue tracker, click on the *New Issue* button. You will see a prefilled issue form, formatted using link:https://www.markdownguide.org/basic-syntax/[markdown] syntax. This image shows a good report that provides full information:


image::/img/help/new-issue-example.png[alt="Example of good report"]


IMPORTANT: *Program version* must be copied by clicking on a button "*Copy Version Info*", found under **Help** -> **About** and pasted into issue form under section "*# KiCad Version*".

== Reporting Locations [[reporting_locations]]

=== KiCad (applications)
If you have a functional issue with using any application be it the project launcher (KiCad),
schematic editor (Eeschema), PCB editor (Pcbnew), Gerber viewer (gerbview) and other executables.

link:https://gitlab.com/kicad/code/kicad/-/issues[I have an application issue^,role="btn btn-default btn-primary"] 

=== Translations (for the applications)
KiCad applications by default have all of their interface text written in English. A group of contributors creates special translation files
to convert the English text to other languages. 


link:https://gitlab.com/kicad/code/kicad-i18n/-/issues[I have a translation issue^,role="btn btn-default btn-primary"] 

=== Documentation (any language)
All the documentation is made up a team of contributors that spend their time painstakingly keeping content up to date with KiCad's changes.
Additionally contributors also spend time translating the same manuals into other languages.

link:https://gitlab.com/kicad/services/kicad-doc/-/issues[I have a documentation issue^,role="btn btn-default btn-primary"] 


=== Website
The website is maintained by another group of maintainers. The main website at kicad.org is separate from the documentation website
docs.kicad.org. Please report any issues in the correct location.



link:https://gitlab.com/kicad/services/kicad-website/issues[I have a kicad.org issue^,role="btn btn-default btn-primary"] 


link:https://gitlab.com/kicad/services/kicad-doc-website/-/issues[I have a docs.kicad.org issue^,role="btn btn-default btn-primary"] 



== Support/Help Requests
Issue trackers are not for general support requests asking how do you use or do something in KiCad.

We ask you instead look at the link:/community/sites/[community forums links] for communities that can provide help.
